package thesis.CryptoLayer;

import java.io.StringReader;
import java.nio.ByteBuffer;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

import thesis.Interfaces.ICrypto;

public class CryptoFunctions implements ICrypto
{
	private UUID uid;
	
	//SHA-256 (bit)
	public byte[] GetHash(byte[] message)
	{ 
		byte[] digest = null; 
		try 
		{ 
			MessageDigest md = MessageDigest.getInstance("SHA-256"); 
			byte[] hash = md.digest(message);
			/*StringBuilder sb = new StringBuilder(hash.length); 
			for(int i = 0; i < hash.length; i++)
			{ 
				sb.append(Integer.toHexString(hash[i] & 0xFF));
			} */
			digest = hash;//sb.toString(); 
		} 
		catch (NoSuchAlgorithmException ex) 
		{ 
			Logger.getLogger(StringReader.class.getName()).log(Level.SEVERE, null, ex); 
		} 
		
		return digest;
	}
	
	//Android
	public UUID GetUUID()
	{
		if (uid == null)
			return UUID.randomUUID();
		else
			return uid;
	}
	
	public UUID GetUUID(byte[] bytes) 
	{
	    ByteBuffer bb = ByteBuffer.wrap(bytes);
	    long firstLong = bb.getLong();
	    long secondLong = bb.getLong();
	    return new UUID(firstLong, secondLong);
	}
	
	public byte[] GetUUID(UUID uuid) 
	{
	    ByteBuffer bb = ByteBuffer.wrap(new byte[16]);
	    bb.putLong(uuid.getMostSignificantBits());
	    bb.putLong(uuid.getLeastSignificantBits());
	    return bb.array();
	 }
	
	public byte[] Random(int count)
	{
		byte[] rezult = new byte[count];
		try 
		{
			SecureRandom sr = SecureRandom.getInstance("SHA1PRNG");
			sr.nextBytes(rezult);
		} 
		catch (NoSuchAlgorithmException e) 
		{
			System.out.println(e.getMessage());
		}
		
		return rezult;
		
	}
}
