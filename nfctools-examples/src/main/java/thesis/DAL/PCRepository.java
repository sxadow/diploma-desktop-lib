package thesis.DAL;

import thesis.Interfaces.IRepository;

import java.io.*;

public class PCRepository implements IRepository
{
	private String DB = "DB.txt";
	
	public PCRepository ()
	{
	}
	
	public PCRepository (String path)
	{
		DB = path;
	}
	
	public void Add(String Nic, byte[] UUID, byte[] secret) throws IOException
	{
		StringBuilder sb = new StringBuilder();
		
		sb.append(Nic);
		sb.append(" ");
		sb.append(BytesToHex(UUID));
		sb.append(" ");
		sb.append(BytesToHex(secret));
		Write(DB, sb.toString());
	}
	
	public byte[] Get(String Nic, byte[] UUID) throws FileNotFoundException
	{
		if (ReadNic(Nic, BytesToHex(UUID)))
			return HexToBytes(Read(DB, BytesToHex(UUID)));
		else
			throw new FileNotFoundException("System doesn`t have this user or incorrect device!");
	}
	
	public boolean ReadNic(String Nic, String UUID)
	{
	    String sb = "";
	    File file = new File(DB);
	 
	    try 
	    {
	    	if(!file.exists())
	        {
	            file.createNewFile();
	        }
	    	
	        BufferedReader in = new BufferedReader(new FileReader( file.getAbsoluteFile()));
	        try 
	        {
	            String s;
	            while ((s = in.readLine()) != null) 
	            {
	                String[] str = s.split(" ", 3);
	                if (str[0].equals(Nic) && str[1].equals(UUID))
	                {
	                	return true;
	                }
	            }
	            
	        } 
	        finally 
	        {
	            in.close();
	        }
	    } 
	    catch(IOException e) 
	    {
	        throw new RuntimeException(e);
	    }
	    
	    return false;
	}
	
	public String GetNic(byte[] UUID)
	{
	    String sb = "";
	    File file = new File(DB);
	 
	    try 
	    {
	    	if(!file.exists())
	        {
	            file.createNewFile();
	        }
	    	
	        BufferedReader in = new BufferedReader(new FileReader( file.getAbsoluteFile()));
	        try 
	        {
	            String s;
	            while ((s = in.readLine()) != null) 
	            {
	                String[] str = s.split(" ", 3);
	                if (str[1].equals(BytesToHex(UUID)))
	                {
	                	sb = str[0];
	                	break;
	                }
	            }
	            
	        } 
	        finally 
	        {
	            in.close();
	        }
	    } 
	    catch(IOException e) 
	    {
	    }	
	    
	    return sb;
	}
	
	//Android
	public byte[] Get()
	{
		byte[] res = {0, 1, 2, 3};
		return res;
	}
	
	final static private char[] hexArray = "0123456789ABCDEF".toCharArray();
	
	private void Write(String fileName, String text) throws IOException 
	{
		FileWriter out = new FileWriter(fileName,true);
	 
	    try 
	    {
	        /*if(!file.exists())
	        {
	            file.createNewFile();
	        }
	 
	        PrintWriter out = new PrintWriter(file.getAbsoluteFile());*/
	        try 
	        {
	            out.write(text);
	            out.write("\n");
	        } 
	        finally 
	        {
	            out.close();
	        }
	    } 
	    catch(IOException e) 
	    {
	        throw new RuntimeException(e);
	    }
	}
	
	private String Read(String fileName, String UUID) throws FileNotFoundException 
	{
		//Этот спец. объект для построения строки
	    String sb = "";
	    File file = new File(fileName);
	 
	    try {
	    	if(!file.exists())
	        {
	            file.createNewFile();
	        }
	    	
	        //Объект для чтения файла в буфер
	        BufferedReader in = new BufferedReader(new FileReader( file.getAbsoluteFile()));
	        try 
	        {
	            //В цикле построчно считываем файл
	            String s;
	            while ((s = in.readLine()) != null) 
	            {
	                String[] str = s.split(" ", 3);
	                if (str[1].equals(UUID))
	                {
	                	sb = str[2];
	                	break;
	                }
	            }
	            
	        } 
	        finally 
	        {
	            //Также не забываем закрыть файл
	            in.close();
	        }
	    } 
	    catch(IOException e) 
	    {
	        throw new RuntimeException(e);
	    }
	    
	    if (sb.equals(""))
	    	throw new FileNotFoundException("I don`t have this uid");
	 
	    //Возвращаем полученный текст с файла
	    return sb;
	}
	
	public static String BytesToHex(byte[] bytes)
	{
		/*StringBuilder sb = new StringBuilder();
		for(int i = 0; i < bytes.length; i++)
		{ 
			sb.append(Integer.toHexString(bytes[i]&0xFF));
		} 
		
		return sb.toString();*/
		char[] hexChars = new char[bytes.length * 2];
	    for ( int j = 0; j < bytes.length; j++ ) 
	    {
	        int v = bytes[j] & 0xFF;
	        hexChars[j * 2] = hexArray[v >>> 4];
	        hexChars[j * 2 + 1] = hexArray[v & 0x0F];
	    }
	    return new String(hexChars);
	}
	
	public static byte[] HexToBytes(String s)
	{
	    int len = s.length();
	    byte[] data = new byte[len / 2];
	    for (int i = 0; i < len; i += 2) 
	    {
	        data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
	                             + Character.digit(s.charAt(i+1), 16));
	    }
	    return data;
	}
}
