package thesis.ProtocolLayer;

import thesis.Interfaces.ICrypto;
import thesis.Interfaces.IReceiver;
import thesis.Interfaces.IRepository;
import thesis.Interfaces.ISender;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Arrays;

public class ProtocolManager implements IReceiver
{
	private IRepository _repository;
	private ICrypto _crypto;
	private ISender _sender;
	private byte[] _uid;
	private byte[] _secret;
	private byte[] _pin;
	private String _login;
	private String nickname;
	private boolean init;
	
	public void StartInit()
	{
		init = true;
	}
	
	public void StopInit()
	{
		init = false;
	}

	@Override
	public String GetUser() {
		return nickname;
	}

	public ProtocolManager(IRepository Repository, ICrypto CryptoFunctions, ISender SendInterface)
	{
		this._repository = Repository;
		this._crypto = CryptoFunctions;
		this._sender = SendInterface;
	}

	public void OnReceive(byte[] message) throws Exception {
		if (message.length == 0)
		{
			throw new IllegalArgumentException();
		}
		if (message[0] >> 4 == 0)
		{
			byte[] result = new byte[1];
			byte[] arg = new byte[message.length - 1];
			System.arraycopy(message, 1, arg, 0, message.length - 1);
			
			switch(message[0])
			{
				case 0: 
					if (init)
					{
						nickname = FuckingNic(arg);
						init = false;
					}
					else
					{
						result = Identity(arg);

						_sender.Send(result);
					}
					break;
				case 1: 
					result = NewPassword(arg);
					_sender.Send(result);
					break;
				case 2: 
					result = Auth(arg);
					_sender.Send(result);
					break;
				case 3: 
					System.out.println("Everithing is awesome");
					break;
				case 4:
					NewSecret(arg);
					break;
				/*default:

					break;*/

			}
			
			if (result[0] == -14)
				throw new Exception("Невірний логін");
			if (result[0] == -13)
				throw new Exception("Невіпний ПІН");
		}
	}
	
	private String FuckingNic(byte[] UUID)
	{
		return _repository.GetNic(UUID);
	}

	public void Login(String Login)
	{
		_login = Login;
	}


	public void NewUser(String Login, byte[] PIN)
	{
//		if (_uid == null)
//		{
//			//throw new Exception("Error");
//		}
		_login = Login;
		_pin = PIN;
	}
	
	private void NewSecret(byte[] guid)
	{
		_uid = guid;
		byte[] random = _crypto.Random(32);
		byte[] r = ConCat(random, _pin);
		byte[] likeOnPC = _crypto.GetHash(r);
        try {
            _repository.Add(_login, _uid, likeOnPC);
        } catch (IOException e) {
            e.printStackTrace();
        }
        byte[] result = new byte[random.length + 1];
		result[0] = 5;
		for (int i = 0; i < random.length; i++)
		{
			result[i + 1] = random[i];
		}

		_sender.Send(result);
	}
	
	private byte[] Identity(byte[] UUID)
	{
		//test
//		byte[] secr = {0, 1, 2};
//		byte[] pin = { 1, 2, 3, 5 };
//		byte[] r = ConCat(secr, pin);
//		byte[] likeOnPC = _crypto.GetHash(r);
//        try {
//            _repository.Add(UUID, likeOnPC);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }

        byte[] result;
		try 
		{
			_uid = UUID;
			_secret = _repository.Get(_login, _uid);
			byte[] random = _crypto.Random(16);
			
			byte[] notkey = ConCat(_secret, random);
			print(notkey);
			_secret = _crypto.GetHash(notkey);
			print(_secret);
			result = new byte[17];
			result[0] = 0x1;
			
			for (int i = 0; i < random.length; i++)
			{
				result[i + 1] = random[i];
			}
			
		} 
		catch (FileNotFoundException e) 
		{
			_uid = null;
			System.out.println(e.getMessage());
			result = new byte[1];
			result[0] = (byte) (0xF << 4 |0x2);
		}
		
		return result;
	}
	
	private byte[] NewPassword(byte[] random)
	{
		byte[] pin = { 1, 2, 3, 5 };
		
		byte[] r = ConCat(_repository.Get(), pin);
		byte[] likeOnPC = _crypto.GetHash(r);
		byte[] notKey = ConCat(likeOnPC, random);
		print(notKey);
		byte[] newkey = _crypto.GetHash(notKey);
		print(newkey);
		
		byte[] result = new byte[33];
		result[0] = 0x2;
		
		for (int i = 0; i < newkey.length; i++)
		{
			result[i + 1] = newkey[i];
		}
		
		return result;
	}
	
	private byte[] Auth(byte[] key)
	{
		byte[] result = { 0x3 };
		
		print(key);
		print(_secret);
		//if (_secret.equals(key))
		if (Arrays.equals(_secret, key))
		//if (_secret.hashCode() == key.hashCode())
			result[0] = 0x3;
		else
			result[0] = (byte) (0xF << 4 | 0x3);
		
		return result;
	}		
		
	
	//only for test!
	private void print(byte[] msg)
	{
		StringBuilder sbl = new StringBuilder();
		for(int i = 0; i < msg.length; i++)
		{ 
			sbl.append(Integer.toHexString(msg[i]&0xFF));
		} 
		System.out.println(sbl.toString());
	}
	
	private byte[] ConCat(byte[] a, byte[] b)
	{
		if (a == null)
			return b;
		if (b == null)
			return a;
		byte[] r = new byte[a.length + b.length];
		System.arraycopy(a, 0, r, 0, a.length);
		System.arraycopy(b, 0, r, a.length, b.length);
		return r;
	}
}
