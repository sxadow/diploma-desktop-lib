package thesis.Interfaces;

public interface IReceiver 
{
	public void OnReceive(byte[] message) throws Exception;

	public void NewUser(String Login, byte[] PIN);
	
	public void Login(String Login);
	
	public void StartInit();
	
	public void StopInit();

	public String GetUser();

}
