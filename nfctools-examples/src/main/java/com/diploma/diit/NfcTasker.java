package com.diploma.diit;

import org.nfctools.examples.llcp.LlcpService;
import org.nfctools.examples.snep.SnepDemo;
import org.nfctools.ndef.Record;
import org.nfctools.ndef.wkt.records.UriRecord;
import org.nfctools.utils.LoggingNdefListener;
import org.nfctools.utils.LoggingStatusListener;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collection;

/**
 * Created by Бодька on 26.05.2015.
 */
   class NfcRunner implements Runnable {
    private String message[] = { "-target", "-url", "FFFFF" };
    private String data;

    NfcRunner()
    {

    }
    NfcRunner(String data)
    {
        this.data = data;
        message[2] = data;
    }
    @Override
    public void run() {
            NfcTasker.WriteToConsole("Snep Runner Started\n");
            SnepDemo demo = new SnepDemo();
            if (data != null)
                demo.addUrlToSend(data);
            try {
                demo.runDemo(false, null);
                demo = null;

            } catch (IOException e) {
                e.printStackTrace();
            }

    }
}
public class NfcTasker {
    //LlcpService service;
    private boolean isActive;
    private String message[] = { "-target", "-url", "FFFFF" };
    private Thread threadNfcRunner;
    private String data;
    private ByteArrayOutputStream outContent;
    private final static PrintStream qwez = new PrintStream(System.out);
    private final static PrintStream qwezError = new PrintStream(System.err);

    public static void WriteToConsole(String text)
    {
        qwez.append(text);
    }
    public NfcTasker(){
//       Thread thread = new Thread(new NfcRunner(data));
        outContent = new ByteArrayOutputStream();
        PrintStream newOut = new PrintStream(outContent);
        ByteArrayOutputStream outContent2 = new ByteArrayOutputStream();
        PrintStream newOut2 = new PrintStream(outContent2);
        System.setOut(newOut);
        System.setErr(newOut2);
    }
    @Override
    protected void finalize() throws Throwable {
        threadNfcRunner.stop();
        System.setOut(qwez);
        System.setErr(qwezError);
        super.finalize();
    }

    public boolean TrySendMessage(String data)
    {
        try {
            return SendMessage(data);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean TrySendMessage(byte[] data){
        String text = new String(data);
        return TrySendMessage(text);
    }

    private boolean SendMessage(String data) throws UnsupportedEncodingException {

        if (threadNfcRunner != null) {
            if(threadNfcRunner.isAlive())
            {
                threadNfcRunner.interrupt();
            }
        }
        message[2] = data;
        threadNfcRunner = new Thread(new NfcRunner(data));
        threadNfcRunner.start();

        while (threadNfcRunner.isAlive()) {
            if (outContent.toString().contains("SNEP succeeded")) {
                qwez.append("Data:\n");
                for (int i=0; i<data.length(); ++i)
                    qwez.printf("%X", data.codePointAt(i));

                threadNfcRunner.interrupt();
            }
            outContent.reset();
        }
        qwez.append("\nSent\n");
        return true;
    }

    public String GetMessage(){
        String tmp = "";
        if(!threadNfcRunner.isAlive()){
            threadNfcRunner = new Thread(new NfcRunner());
            threadNfcRunner.start();
        }
        while (threadNfcRunner.isAlive()) {
            tmp = FindData(outContent.toString());
            if (!tmp.isEmpty()) {
                qwez.append(("\nReceived\n"));
                threadNfcRunner.interrupt();
            }
            outContent.reset();
        }
        return tmp;
    }

    private final static String FindData(String tmp) {
        final String startS = "02656E";
        final String endS = "2D53";
        int start = tmp.indexOf(startS);
        int end = tmp.indexOf("2D53");
        if (start < end) {
            return tmp.substring(start + startS.length(), end);
        }
        return "";
    }

    public static void main(String[] args){
//        NfcTasker nfct = new NfcTasker();
   /*     NfcTasker nfct = new NfcTasker();
        nfct.TrySendMessage("Hi!");
     //   NfcTasker.WriteToConsole(nfct.GetMessage());
//        try {
//      //     nfct.finalize();
//        } catch (Throwable throwable) {
//            throwable.printStackTrace();
//        }
        try {
            nfct = null;
            Thread.currentThread().setPriority();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


        nfct = new NfcTasker();
        nfct.TrySendMessage("You");

        try {
            nfct = null;
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        nfct = new NfcTasker();
       // NfcTasker.WriteToConsole(nfct.GetMessage());
        nfct.TrySendMessage("Fucken");
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        nfct = null;

        nfct = new NfcTasker();
      //  NfcTasker.WriteToConsole(nfct.GetMessage());
        nfct.TrySendMessage("Retard!");*/
        final Collection<Record> azazaza = new ArrayList<Record>();
        azazaza.add(new UriRecord("HELLO FUCKER!"));
        azazaza.add(new UriRecord("HELLO FLUCKER!"));
        final LlcpService service = new LlcpService(new LoggingNdefListener(), new LoggingStatusListener());
//        service.run2();
        new Thread(new Runnable() {

            @Override
            public void run() {
                try {
//                    Thread.sleep(1000);
                    System.out.println("NEW THREAD\n\n\n\n");
                    service.run2();
                    Thread.sleep(10000);
                    System.out.println("\n\n\nAdding new jjjjrecords...");
                    service.addMessages(azazaza, null);
                //    addMessages(records, null);
                }
                catch (InterruptedException e) {
                }
            }
        }).start();
        try {
            System.in.read();
        } catch (IOException e) {
            e.printStackTrace();
        }
        new Thread(new Runnable() {

            @Override
            public void run() {
                try {
                    Thread.sleep(5000);
                    System.out.println("############NEW THREAD\n\n#\n\n");

                    System.out.println("\n\n\n#####Adding new jjjjrecords...");
                    service.addMessages(azazaza, null);
                    //    addMessages(records, null);
                }
                catch (InterruptedException e) {
                }
            }
        }).start();

//        try {
//            Thread.sleep(5000);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
        System.out.println("#########################################################################");


     //   nfct.service.addMessages(azazaza,null);
        System.out.println("*******************************************************************");

        //service.run2();
    }
}
