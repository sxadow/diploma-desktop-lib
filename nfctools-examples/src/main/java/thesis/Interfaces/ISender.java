package thesis.Interfaces;

public interface ISender 
{
	public void Send(byte[] message);
}
