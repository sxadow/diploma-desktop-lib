package com.diploma.diit;

/**
 * Created by Бодька on 28.05.2015.
 */
public interface NetworkServiceInterface {

    /**
     * Like {@link #addMessageToQueue(String data)} but with byte[] param
     */
    public void addMessageToQueue(byte[] data);

    /**
     * Sending message without block
     *
     * @param data
     */
    public void addMessageToQueue(String data);

    /**
     * Sending message with block
     *
     * @param data
     */
    public void sendMessage(String data);

    /**
     * Like {@link #sendMessage(String data)} but with byte[] param
     */
    public void sendMessage(byte[] data);

    /**
     * Getting message from nfc with block
     *
     * @return TextData
     */
    public String getMessage();

    /**
     * Writing text to console
     *
     * @param text
     */
    public void writeToConsole(String text);

    /**
     * Get Message with lock in byte;
     */
    public byte[] getMessageByte();
}
