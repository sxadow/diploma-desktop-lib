/*
 * Created by JFormDesigner on Fri Jun 05 23:34:41 EEST 2015
 */

package myProj;

import com.diploma.diit.NfcConnectionManager;
import thesis.CryptoLayer.CryptoFunctions;
import thesis.DAL.PCRepository;
import thesis.ProtocolLayer.ProtocolManager;

import javax.smartcardio.CardException;
import javax.smartcardio.CardTerminal;
import javax.smartcardio.TerminalFactory;
import javax.swing.*;
import javax.swing.border.BevelBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * @author sxadow
 */
public class Main extends JFrame {


    private ProtocolManager protocol;
    private NfcConnectionManager sender;

    public Main() {
        initComponents();
        initMyConfigs();
    }

    private void lockAllComponents() {
        buttonLogin.setEnabled(false);
        radioButtonAuth.setEnabled(false);
        radioButtonRegistration.setEnabled(false);
        radioButtonIdentification.setEnabled(false);
        passwordFieldPIN.setEnabled(false);
    }
    private void resetLog(){
        progressBar1.setValue(0);
        progressBar1.setVisible(false);
        labelMessage.setText("");
        labelMessage.setVisible(false);
    }
    private void unlockAllComponents() {
        buttonLogin.setEnabled(true);
        radioButtonAuth.setEnabled(true);
        radioButtonRegistration.setEnabled(true);
        radioButtonIdentification.setEnabled(true);
        passwordFieldPIN.setEnabled(true);

    }

    private void identification(){
        protocol.StartInit();
        byte[] qwe = sender.getMessageByte();
        try {
            protocol.OnReceive(qwe);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(this, "Помилка при ідентифікації:\n" + e.getMessage());
			protocol.StopInit();
            unlockAllComponents();
            resetLog();
            return;
        }
        labelMessage.setVisible(true);
        labelMessage.setText(protocol.GetUser());
        unlockAllComponents();

    }
    private void authenticatoin(){
        String tmp = textFieldLogin.getText();
        if (tmp.contains(" ")) {
            JOptionPane.showMessageDialog(this, "Логін не повинен містити пробіл");
            return;
        }
        protocol.Login(tmp);
        sender.writeToConsole("Authentication Started\n\n");
        progressBar1.setValue(5);
        labelMessage.setText("Очікування токену");
        byte[] qwe = sender.getMessageByte();
        progressBar1.setValue(25);
        labelMessage.setText("Обмін даними");
        sender.writeToConsole("Received " + PCRepository.BytesToHex(qwe));

        try {
            protocol.OnReceive(qwe);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(this, "Помилка при автентифікації:\n"+e.getMessage());
            resetLog();
            unlockAllComponents();
            return;
        }

        progressBar1.setValue(50);
        qwe = sender.getMessageByte();
        progressBar1.setValue(75);
        sender.writeToConsole("Received " + PCRepository.BytesToHex(qwe));
        try {
            protocol.OnReceive(qwe);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(this, "Помилка при автентифікації:\n"+e.getMessage());
            resetLog();
            unlockAllComponents();
            return;
        }
        progressBar1.setValue(100);
        labelMessage.setText("Кінець.");
        unlockAllComponents();
    }

    public byte[] parsePin(char[] tmp) throws Exception {
        byte[] pwd = new byte[tmp.length];
        if(tmp.length<4)
            throw new Exception("PIN має бути не менше чотирьох цифр");
        for (int i = 0; i < tmp.length ; ++i){
            pwd[i] = (byte) (tmp[i] - 48);
            if (pwd[i] > 9)
                throw new Exception("PIN має містити тільки цифри");
        }
        return pwd;
    }

    private void registration(){
        String login = textFieldLogin.getText();
        if (login.contains(" ")) {
            JOptionPane.showMessageDialog(this, "Логін не повинен містити пробіл");
            return;
        }
        byte [] pin;
        try {
            pin = parsePin(passwordFieldPIN.getPassword());

        } catch (Exception e) {
            JOptionPane.showMessageDialog(this, e.getMessage());
            unlockAllComponents();
            resetLog();
            return;
        }
        protocol.NewUser(login, pin);
        sender.writeToConsole("Waiting for message");
        sender.writeToConsole("Registration Started\n\n");
        progressBar1.setValue(15);
        labelMessage.setText("Очікування токену");
        byte[] qwe = sender.getMessageByte();
        progressBar1.setValue(60);
        labelMessage.setText("Обмін даними");
        sender.writeToConsole("Received " + PCRepository.BytesToHex(qwe));
        try {
            protocol.OnReceive(qwe);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(this, "Помилка при регістрації:\n" + e.getMessage());
            unlockAllComponents();
            return;
        }
        progressBar1.setValue(100);
        labelMessage.setText("Кінець.Прийміть на токені");
        unlockAllComponents();
    }

    class ActionBtnLogin implements ActionListener{


        @Override
        public void actionPerformed(ActionEvent e) {


            labelMessage.setVisible(true);
            lockAllComponents();

            progressBar1.setVisible(true);
            new Thread(new Runnable() {

                @Override
                public void run() {
                    if (radioButtonAuth.isSelected()){
                        authenticatoin();
                    }
                    else {
                        if (radioButtonRegistration.isSelected()) {
                            registration();
                        }else {
                            identification();
                        }
                    }
                }
            }).start();
//            qwe.start();;
//            try {
//                qwe.isAlive()
//            } catch (InterruptedException e1) {
//                e1.printStackTrace();
//            }
        }
    }

    class ActionBtnRegistration implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {
//            panelRegistration.setVisible(true);
          //  panelAuthentication.setVisible(false);
          //  radioButtonRegistration.setEnabled(true);
            resetLog();
            labelLoginPIN.setVisible(true);
            textFieldLogin.setVisible(true);
            labelLogin2.setVisible(true);
            passwordFieldPIN.setVisible(true);
            radioButtonAuth.setSelected(false);
            radioButtonRegistration.setSelected(true);
            radioButtonIdentification.setSelected(false);

        }
    }

    class ActionBtnAuthentication implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {
          //  panelAuthentication.setVisible(true);
//            panelRegistration.setVisible(false);
       //     radioButtonAuth.setEnabled(false);
            resetLog();
            radioButtonRegistration.setSelected(false);
            radioButtonAuth.setSelected(true);
            labelLogin2.setVisible(true);
            textFieldLogin.setVisible(true);
            labelLoginPIN.setVisible(false);
            passwordFieldPIN.setVisible(false);
            radioButtonIdentification.setSelected(false);
        }
    }

    class ActionBtnIdentificator implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {
            //  panelAuthentication.setVisible(true);
//            panelRegistration.setVisible(false);
            //     radioButtonAuth.setEnabled(false);
            resetLog();
            radioButtonRegistration.setSelected(false);
            radioButtonAuth.setSelected(false);
            radioButtonIdentification.setSelected(true);
            labelLoginPIN.setVisible(false);
            labelLogin2.setVisible(false);
            textFieldLogin.setVisible(false);
            passwordFieldPIN.setVisible(false);


        }
    }

    private void initMyConfigs() {
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        radioButtonAuth.addActionListener(new ActionBtnAuthentication());
        radioButtonRegistration.addActionListener(new ActionBtnRegistration());
        radioButtonIdentification.addActionListener(new ActionBtnIdentificator());
        buttonMakeConnection.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                TerminalFactory factory;
                java.util.List<CardTerminal> terminals;
                factory = TerminalFactory.getDefault();
                System.out.println("Get terminals");
                try {
                    terminals = factory.terminals().list();
                    if (terminals.size() == 0) {
                        JOptionPane.showMessageDialog(null, "NFC зчитувач не знайден");
                        return;
                    }
                } catch (CardException e1) {
                    JOptionPane.showMessageDialog(null, "NFC зчитувач не знайден");
                    return;
                }

                sender = new NfcConnectionManager();
                protocol = new ProtocolManager(new PCRepository(), new CryptoFunctions(), sender);
                buttonMakeConnection.setEnabled(false);
                buttonLogin.setEnabled(true);
            }
        });
        buttonLogin.setEnabled(false);
        labelLoginPIN.setVisible(false);
        passwordFieldPIN.setVisible(false);
        progressBar1.setVisible(false);
        Font font = new Font("Ubuntu", Font.BOLD, 18);
        labelMessage.setFont(font);
        labelMessage.setVisible(false);
        buttonLogin.addActionListener(new ActionBtnLogin());
    }
    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        // Generated using JFormDesigner Evaluation license - qwe asd
        panelLogin = new JPanel();
        labelLogin2 = new JLabel();
        labelLoginPIN = new JLabel();
        textFieldLogin = new JTextField();
        buttonLogin = new JButton();
        passwordFieldPIN = new JPasswordField();
        panel3 = new JPanel();
        radioButtonAuth = new JRadioButton();
        radioButtonRegistration = new JRadioButton();
        radioButtonIdentification = new JRadioButton();
        labelMessage = new JLabel();
        progressBar1 = new JProgressBar();
        buttonMakeConnection = new JButton();

        //======== this ========
        Container contentPane = getContentPane();
        contentPane.setLayout(null);

        //======== panelLogin ========
        {

            // JFormDesigner evaluation mark

            panelLogin.setLayout(null);

            //---- labelLogin2 ----
            labelLogin2.setText("\u0412\u0432\u0435\u0434\u0456\u0442\u044c \u043b\u043e\u0433\u0456\u043d");
            panelLogin.add(labelLogin2);
            labelLogin2.setBounds(10, 15, 95, labelLogin2.getPreferredSize().height);

            //---- labelLoginPIN ----
            labelLoginPIN.setText("\u0412\u0432\u0435\u0434\u0456\u0442\u044c PIN");
            panelLogin.add(labelLoginPIN);
            labelLoginPIN.setBounds(10, 70, 95, 15);
            panelLogin.add(textFieldLogin);
            textFieldLogin.setBounds(10, 40, 90, textFieldLogin.getPreferredSize().height);

            //---- buttonLogin ----
            buttonLogin.setText("\u041e\u041a");
            panelLogin.add(buttonLogin);
            buttonLogin.setBounds(5, 135, 135, buttonLogin.getPreferredSize().height);
            panelLogin.add(passwordFieldPIN);
            passwordFieldPIN.setBounds(10, 100, 90, passwordFieldPIN.getPreferredSize().height);

            { // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panelLogin.getComponentCount(); i++) {
                    Rectangle bounds = panelLogin.getComponent(i).getBounds();
                    preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                    preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panelLogin.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panelLogin.setMinimumSize(preferredSize);
                panelLogin.setPreferredSize(preferredSize);
            }
        }
        contentPane.add(panelLogin);
        panelLogin.setBounds(5, 5, 225, 190);

        //======== panel3 ========
        {
            panel3.setLayout(new FlowLayout(FlowLayout.LEFT));

            //---- radioButtonAuth ----
            radioButtonAuth.setText("\u0410\u0432\u0442\u0435\u043d\u0442\u0438\u0444\u0456\u043a\u0430\u0446\u0456\u044f");
            radioButtonAuth.setSelected(true);
            panel3.add(radioButtonAuth);

            //---- radioButtonRegistration ----
            radioButtonRegistration.setText("\u0420\u0435\u0454\u0441\u0442\u0440\u0430\u0446\u0456\u044f");
            panel3.add(radioButtonRegistration);

            //---- radioButtonIdentification ----
            radioButtonIdentification.setText("\u0406\u0434\u0435\u043d\u0442\u0438\u0444\u0456\u043a\u0430\u0446\u0456\u044f");
            panel3.add(radioButtonIdentification);
        }
        contentPane.add(panel3);
        panel3.setBounds(240, 15, 130, 110);

        //---- labelMessage ----
        labelMessage.setText("\u041e\u0447\u0456\u043a\u0443\u0432\u0430\u043d\u043d\u044f \u0434\u0430\u043d\u0438\u0445");
        labelMessage.setBorder(new BevelBorder(BevelBorder.LOWERED));
        labelMessage.setHorizontalTextPosition(SwingConstants.LEADING);
        contentPane.add(labelMessage);
        labelMessage.setBounds(70, 270, 280, 55);
        contentPane.add(progressBar1);
        progressBar1.setBounds(new Rectangle(new Point(135, 250), progressBar1.getPreferredSize()));

        //---- buttonMakeConnection ----
        buttonMakeConnection.setText("\u0417'\u0454\u0434\u043d\u0430\u043d\u043d\u044f \u0437 NFC");
        contentPane.add(buttonMakeConnection);
        buttonMakeConnection.setBounds(new Rectangle(new Point(290, 180), buttonMakeConnection.getPreferredSize()));

        { // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < contentPane.getComponentCount(); i++) {
                Rectangle bounds = contentPane.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = contentPane.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            contentPane.setMinimumSize(preferredSize);
            contentPane.setPreferredSize(preferredSize);
        }
        setSize(475, 375);
        setLocationRelativeTo(getOwner());
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    // Generated using JFormDesigner Evaluation license - qwe asd
    private JPanel panelLogin;
    private JLabel labelLogin2;
    private JLabel labelLoginPIN;
    private JTextField textFieldLogin;
    private JButton buttonLogin;
    private JPasswordField passwordFieldPIN;
    private JPanel panel3;
    private JRadioButton radioButtonAuth;
    private JRadioButton radioButtonRegistration;
    private JRadioButton radioButtonIdentification;
    private JLabel labelMessage;
    private JProgressBar progressBar1;
    private JButton buttonMakeConnection;
    // JFormDesigner - End of variables declaration  //GEN-END:variables

    public static void main(String[] args) {

        Main frame = new Main();
        frame.setVisible(true);
        frame.setResizable(false);
    }
}
