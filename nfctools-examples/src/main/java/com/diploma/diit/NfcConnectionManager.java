package com.diploma.diit;

import org.nfctools.examples.llcp.LlcpService;
import org.nfctools.ndef.Record;
import org.nfctools.ndef.wkt.records.UriRecord;
import org.nfctools.utils.LoggingNdefListener;
import org.nfctools.utils.LoggingStatusListener;
import org.nfctools.utils.NfcUtils;
import thesis.DAL.PCRepository;
import thesis.Interfaces.ISender;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collection;


public class NfcConnectionManager implements NetworkServiceInterface, ISender {
    private final LlcpService service;
    volatile  private ByteArrayOutputStream outContent;
    volatile private PrintStream newOut;
    private final static PrintStream qwez = new PrintStream(System.out);
    private final static PrintStream qwezError = new PrintStream(System.err);
    public final String SEND_MESSAGE_TEXT = "Message send";

    public void Send(byte[] message)
    {
        outContent.reset();
    	addMessageToQueue(PCRepository.BytesToHex(message));
    }

    public NfcConnectionManager() {
//        outContent = new ByteArrayOutputStream();
//        try {
//            newOut = new PrintStream(new FileOutputStream("D:\\log.txt", true));
//            System.setOut(newOut);
//        } catch (FileNotFoundException e) {
//            e.printStackTrace();
//        }
        outContent = new ByteArrayOutputStream();
        newOut = new PrintStream(outContent);
        System.setOut(newOut);
        ByteArrayOutputStream outContent2 = new ByteArrayOutputStream();
        PrintStream newOut2 = new PrintStream(outContent2);

        System.setErr(newOut2);
        service = new LlcpService(new LoggingNdefListener(), new LoggingStatusListener());

        new Thread(new Runnable() {

            @Override
            public void run() {
                qwez.println("Creating service Started\n\n");
                service.run2();
            }
        }).start();
    }

    @Override
    protected void finalize() throws Throwable {
        System.setOut(qwez);
        System.setErr(qwezError);
        super.finalize();
    }

    public void addMessageToQueue(byte[] data) {
        String text = new String(data);
        addMessageToQueue(text);
    }

    public void addMessageToQueue(final String  data) {
        final Collection<Record> message = new ArrayList<Record>();
        message.add(new UriRecord(data));
        qwez.println("\n\nAdding new records..." + data);
        service.addMessages(message, null);
        qwez.println("Messeg Added\n\n");
    }

    public void sendMessage(byte[] data) {
        sendMessage(PCRepository.BytesToHex(data));
    }

    public void sendMessage(String data) {
        outContent.reset();
        addMessageToQueue(data);
        waitingWhenMessageSend();
    }

    /**
     * wait until reader send message
     */
    @Deprecated
    private void waitingWhenMessageSend() {
        qwez.println("Waiting when message sent");
        boolean isSent = false;
        while (!isSent) {
            if (outContent.toString().contains(SEND_MESSAGE_TEXT)) {
                qwez.println("Sent");
                isSent = true;
            }
            outContent.reset();
        }
    }


    public String getMessage() {
        outContent.reset();
        String tmp;
        String result = "";
        while (result.isEmpty()) {
            tmp = outContent.toString();
            outContent.reset();
            qwez.append(tmp);

            result = findData(tmp);
            if(result.isEmpty())
            {
                result = findDataNew(tmp);
            }
            if (!result.isEmpty()) {
                outContent.reset();
                qwez.append(("\n\nReceived\n"));

            }
        //   outContent.reset();
        }
        return result;
    }

    public final static String findDataNew(String tmp){
        final String startS = "Text: ";
        final String endS = ", Locale: en";
        int start = tmp.indexOf(startS);
        int end = tmp.indexOf(endS);
        if (start < end) {
            return tmp.substring(start + startS.length(), end);
        }
        return "";
    }

    public final static String findData(String tmp) {
        final String startS = "02656E";
        final String endS = "\n";
        if(tmp.contains("D58700832000010000000101000000"))
        {
            qwez.print("\n\n\nStart find");
        }
        int start = tmp.indexOf(startS);
        if(start>=0) {
            int end = tmp.substring(start).indexOf(endS) + start;
            if (start < end) {
                qwez.print("\n\n\nReturning");
                String qwe = new String(NfcUtils.convertASCIIToBin(tmp.substring(start + startS.length(), end)));
                qwez.print(qwe);
                return qwe ;
            }

        }
        return "";
    }

    public void writeToConsole(String text) {
        qwez.append(text);
    }

    @Override
    public byte[] getMessageByte() {
        return PCRepository.HexToBytes(getMessage());
    }

    public static void main(String args[]) {

        System.out.println(NfcConnectionManager.findData("DEBUG: org.nfctools.spi.acs.ApduReaderWriter - command  APDU => FF00000002D486\n" +
                "DEBUG: org.nfctools.spi.acs.ApduReaderWriter - response APDU <= D5870083200001000000010100000029D101255402656E30303843343833314331374533303433313942373143383241313943313044464230\n"));
        NfcConnectionManager ncm = new NfcConnectionManager();
        while(true) {
            try {
                System.in.read();
                //    ncm.writeToConsole("Sending:\n\" GIVE ME THE TITS\n");
                ncm.writeToConsole("0BJAD");
                ncm.addMessageToQueue("04F6D0A94D6E45107C00494ABBA98DA79557653222D10C57F8F5A0C4C39642F0D8");
//                System.in.read();
//            ncm.writeToConsole("1BJAD");
//           ncm.getMessage();
                ncm.writeToConsole("Message bytes:\n" + ncm.getMessage());
                ncm.writeToConsole("\n2BJAD");
//                System.in.read();
                ncm.addMessageToQueue("04F6D0A94D6E45107C00494ABBA98DA79557653222D10C57F8F5A0C4C39642F0D8");
                ncm.writeToConsole("Message bytes:\n" + ncm.getMessage());
                ncm.writeToConsole("2BJAD");
//                System.in.read();
                ncm.addMessageToQueue("05F6D0A94D6E45107C00494ABBA98DA79557653222D10C57F8F5A0C4C39642F0D8");
                ncm.writeToConsole("Message bytes:\n" + ncm.getMessage());
                ncm.writeToConsole("2BJAD");
//                System.in.read();
                ncm.addMessageToQueue("06F6D0A94D6E45107C00494ABBA98DA79557653222D10C57F8F5A0C4C39642F0D8");
                ncm.writeToConsole("Message bytes:\n" + ncm.getMessage());
                ncm.writeToConsole("2BJAD");
//                System.in.read();
                ncm.addMessageToQueue("07F6D0A94D6E45107C00494ABBA98DA79557653222D10C57F8F5A0C4C39642F0D8");
                ncm.writeToConsole("Message bytes:\n" + ncm.getMessage());
                ncm.writeToConsole("2BJAD");

//            ncm.writeToConsole("Message bytes:\n" + ncm.getMessage());
//            ncm.writeToConsole("2BJAD");
//            ncm.writeToConsole("Message bytes:\n" + ncm.getMessage());
//            ncm.writeToConsole("2BJAD");
                //     System.in.read();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
    //  NfcConnectionManager
}
