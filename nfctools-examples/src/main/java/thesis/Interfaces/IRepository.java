package thesis.Interfaces;

import java.io.FileNotFoundException;
import java.io.IOException;

public interface IRepository 
{
	public void Add(String Nic, byte[] UUID, byte[] secret) throws IOException;
	
	public byte[] Get();
	
	public byte[] Get(String Nic, byte[] UUID) throws FileNotFoundException;
	
	public String GetNic(byte[] UUID);
}
